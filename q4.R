library(dplyr)
library(stringi)


## nazwy użytkowników obecne na wszystkich stackach
a1 <- hiszp[[7]] %>% 
  inner_join(jap[[7]], by = "DisplayName") %>%
  inner_join(niem[[7]], by = "DisplayName") %>%
  inner_join(fra[[7]], by = "DisplayName") %>%
  inner_join(kor[[7]], by = "DisplayName") %>%
  inner_join(chin[[7]], by = "DisplayName") %>%
  select(DisplayName) %>%
  # nazwy typu "Imię"
  filter(stri_count(DisplayName, regex = "^\\p{Lu}\\p{Ll}+$") == 0) %>%
  # nazwy typu "Imię Nazwisko"
  filter(stri_count(DisplayName, regex = "^\\p{Lu}\\p{Ll}+ \\p{Lu}\\p{Ll}+$") == 0) %>%
  # powtarzające się proste nazwy
  filter(!(DisplayName %in% c("user", "guest", "anonymous")))


a2 <- a1 %>%
  inner_join(por[[7]], by = "DisplayName") %>%
  inner_join(ros[[7]], by = "DisplayName") %>%
  inner_join(ukr[[7]], by = "DisplayName") %>%
  select(DisplayName)
  

a2 %>% arrange(DisplayName)



## najpopularniejsze tagi
fra[[6]] %>%
  mutate(Count = as.numeric(Count)) %>%
  arrange(desc(as.numeric(Count))) %>%
  select(TagName) %>%
  head(20)







