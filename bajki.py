from bs4 import BeautifulSoup
import requests

#
# df_file = open('manga.csv', 'w+')
# n_pages = 10
#
# print("rank;type;title", file=df_file)
#
# rank = 1
# for page in range(0, n_pages):
#     page_link = "https://myanimelist.net/topmanga.php?type=bypopularity&limit=" + str(page * 50)
#     page_html = requests.get(page_link).content
#     page_soup = BeautifulSoup(page_html)
#     for a in page_soup.find_all('a'):
#         if str(a.get('id')).startswith("#area") and a.get('class')[1] == "fs14":
#             print(rank)
#             anime_link = a.get('href')
#             print(anime_link)
#             anime_html = requests.get(anime_link).content
#             anime_soup = BeautifulSoup(anime_html)
#             for div in anime_soup.find_all('div'):
#                 if div.get('class') == ["spaceit_pad"]:
#                     text = div.get_text().strip()
#                     # Angielski tytuł
#                     if text.startswith("English: "):
#                         title = text[9:]
#                         type = "ang"
#                         print(str(rank) + ";" + type + ";" + title, file=df_file)
#
#                     # Japoński tytuł
#                     if text.startswith("Japanese: "):
#                         title = text[10:]
#                         type = "jap"
#                         print(str(rank) + ";" + type + ";" + title, file=df_file)
#
#                     # Synonimy
#                     if text.startswith("Synonyms: "):
#                         title = text[10:]
#                         type = "syn"
#                         for syn in title.split(", "):
#                             print(str(rank) + ";" + type + ";" + syn, file=df_file)
#
#             rank += 1




## anime
df_file = open('anime.csv', 'w+')
n_pages = 10

print("rank;type;title", file=df_file)

rank = 1
for page in range(0, n_pages):
    page_link = "https://myanimelist.net/topanime.php?type=bypopularity&limit=" + str(page * 50)
    page_html = requests.get(page_link).content
    page_soup = BeautifulSoup(page_html)
    for a in page_soup.find_all('a'):
        if str(a.get('id')).startswith("#area") and a.get('class')[2] == "fs14":
            print(rank)
            anime_link = a.get('href')
            print(anime_link)
            anime_html = requests.get(anime_link).content
            anime_soup = BeautifulSoup(anime_html)
            # print(anime_soup.prettify())
            for div in anime_soup.find_all('div'):
                if div.get('class') == ["spaceit_pad"]:
                    text = div.get_text().strip()
                    # print(text)
                    # Angielski tytuł
                    if text.startswith("English: "):
                        title = text[9:]
                        type = "ang"
                        print(str(rank) + ";" + type + ";" + title, file=df_file)

                    # Japoński tytuł
                    if text.startswith("Japanese: "):
                        title = text[10:]
                        type = "jap"
                        print(str(rank) + ";" + type + ";" + title, file=df_file)

                    # Synonimy
                    if text.startswith("Synonyms: "):
                        title = text[10:]
                        type = "syn"
                        for syn in title.split(", "):
                            print(str(rank) + ";" + type + ";" + syn, file=df_file)

            rank += 1

